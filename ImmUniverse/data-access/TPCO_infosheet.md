---
layout: default
title: Infosheet for Third Party Cohorts
parent: Data Access
nav_order: 6
---

# ImmUniverse Infosheet and Third Party Cohort Data Access
{: .no_toc }

This document aims to help a cohort representative to reach out to their third-party cohort owner to start the data access ball rolling. Please feel free to use whichever part of this document is helpful.


{: .no_toc .d-inline-block}
coming soon
{: .label .label-yellow }