---
layout: default
title: Data Security
nav_order: 5
has_children: false
permalink: /data-security
---

# Data Security

UL, a ImmUniverse partner, provides a secure and modern environment to store, process, and access various type of data collected or to be collected in the ImmUniverse project.

UL only receives the sensitive data that are pseudonymised or anonymised by the Data Provider and encrypted during the transfer and at the client-side. Transmission of data occurs over an end-to-end encrypted, high-speed channel, called LCSB Data Upload Manager (DUMA) and built on IBM’s Aspera Technology. UL has defined policy for encryption of sensitive data, working disk, and backups for data at rest.

UL's Computing platforms - servers and storage devices are placed at a highly secured data centre with control access and video surveillance. Physical access to any data centre resource requires multiple approval steps. Digital access is limited to authorised users and restricted number of UL System Administrators. OS and application authentications are required for any access and all activities are logged.

User role and access levels are defined depending on the legal agreements at both consortium and individual levels. For each dataset, a list of users who have access to the dataset is defined and maintained by UL. Each person can only access the data to which they have a right of access.

A data backup concept is defined and implemented. Backup of the Raw data is present on a different server with flooding and fire protection.

Temporary copies of the data are permanently erased from any storage device before recycling. Internet-facing systems are regularly tested against vulnerabilities in accordance with the UL Information Management Security Policy and in collaboration with the Chief Information Security Officer of the UL.

An IT Code of Conduct in accordance to UL's IT Security and Privacy policy is defined and implemented internally. UL has an assigned Data Protection Officer (DPO) and an online system to record the access rights and legal agreements for any dataset hosted in UL.

Further, UL has a procedure to record and report data breaches to the national authority. UL regularly conducts the IT risk assessments, and mandatory data protection trainings for all employees working with sensitive data. UL has strict zero tolerance policy in case of probable data breach or threat of such.
