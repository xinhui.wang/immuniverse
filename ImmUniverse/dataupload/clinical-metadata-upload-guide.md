---
layout: default
title: Clinical Metadata
parent: Metadata Upload Guide
grand_parent: Data Upload
nav_order: 1
---

# Clinical metadata
{: .no_toc}

Work packages 2?3?5 and 6 include the activity of harmonising and merging
phenotype data, multi-omics data and information about biological
samples for existing and upcoming patient and population cohorts within
ImmUniverse. In order to facilitate this task, detailed metadata about the
cohorts needs to be collected.



## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

{: .no_toc .d-inline-block}
coming soon
{: .label .label-yellow }