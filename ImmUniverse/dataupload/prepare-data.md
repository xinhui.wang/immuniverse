---
layout: default
title: Prepare data for upload
parent: Data Upload Guide
grand_parent: Data Upload
nav_order: 1
---

# Prepare data for upload
{: .no_toc }

This document provides instructions how to prepare the clinical and omics data.


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

{: .no_toc .d-inline-block}
coming soon
{: .label .label-yellow }