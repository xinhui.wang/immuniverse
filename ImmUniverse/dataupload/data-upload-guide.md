---
layout: default
title: Data Upload Guide
parent: Data Upload
has_children: true
nav_order: 1
---

# Data Upload Guide
{: .no_toc }

This document provides instructions how to upload the data (containing clinical and omics data). Data owners should follow the workflow, illustrated below to [prepare]({% link ImmUniverse/dataupload/prepare-data.md %}) and [upload]({% link ImmUniverse/dataupload/upload-data.md %}) their data to UL data platform.

## Data upload workflow in a nutshell

```mermaid
graph LR
  A(Prepare<br />data) --> B(Create a <br />ticket <br />for data<br />upload link)
  B --> C(Upload<br />Data<br />Protection<br />Information<br />Sheet<br />or Appendix B)
  C --> C1(Submit<br />the<br />ticket)
  C1 --> |Check<br />email| D(Open<br />data<br />upload<br />link)
  D --> E(Upload<br />data)

```



### Quick links

- [Create a ticket for data upload link](https://ImmUniverse-redcap.lcsb.uni.lu/redcap/surveys/?s=39DXWAEEP8){:target='new'}
- [Data Protection Information Sheet]({{ site.baseurl }}/assets/docs/UL_Extended_Data_Information_Sheet-8.xlsx)



### Overview video

<script src="https://fast.wistia.com/embed/medias/zm25le2l93.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_zm25le2l93 popover=true popoverAnimateThumbnail=true" style="display:inline-block;height:84px;position:relative;width:150px">&nbsp;</span>
