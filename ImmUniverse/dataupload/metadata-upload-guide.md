---
layout: default
title: Metadata Upload Guide
parent: Data Upload
has_children: true
nav_order: 2
---

# Metadata Upload Guide
{: .no_toc}

Work packages 2, 3, 5 and 6 include the activity of harmonising and merging
phenotype data, multi-omics data and information about biological
samples for existing and upcoming patient and population cohorts within
ImmUniverse. In order to facilitate this task, detailed metadata about the
cohorts needs to be collected.


## Overview


The first section of this document will guide you in uploading such
metadata about your cohorts to *`ImmUniversep*, the system in use to
systematically capture this information.

**Data sharing**

Note that the metadata does not contain any phenotypic or molecular data
on the individual level, and therefore can be shared without data
processing/sharing agreement.
