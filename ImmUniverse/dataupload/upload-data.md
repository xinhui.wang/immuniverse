---
layout: default
title: Uploading Data into the ImmUniverse Platform
parent: Data Upload Guide
grand_parent: Data Upload
nav_order: 2
---

# Uploading Data into the ImmUniverse Platform
{: .no_toc }

In order to securely upload data into the platform we have developed a secure mechanism that can be accessed via a secure link sent via email.

```mermaid

flowchart LR
  A(fa:fa-user-circle <br />Data uploader) --> C{fa:fa-creative-commons-pd-alt<br />Is it<br />Public<br />Data?}
  A ==> |"Data"| format["Format Data<br />according to<br />the formatting<br />guide-lines"]
  subgraph Legal Requirements  
  C --> |No| F["fa:fa-file-excel Fill out <br />DISH or<br />Appendix B"]
  fg("Contact DPO<br />of your<br />institute<br />or UNIVIE<br />team") --> E["Complete <br /> & verify<br />legal<br />sharing<br />status"]
  end
  C -->|Yes| G["Create<br />ticket for<br />data<br />upload"]
  F --> G
  G --> |Check<br />Email| email["Open<br />data<br />upload<br />link"]
  email --> H["fa:fa-upload Upload<br />data"]
  A ==> |"Meta<br />data"| format
  format ==> id[Add<br />ImmUniverse IDs<br />to subjects<br />and samples]
  id ==> |"For Data<br />Upload help<br />contact<br />UL team"| H
```

ImmUniverse Data Upload Flowchart:  This figure outlines various steps need to fulfil to deposit the data into data computing environment at UL.


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

### Steps and requirements for accessing the data upload link

Prior to uploading any clinical or molecular data files, it is mandatory to comply with the ImmUniverse’s legal requirements and provide data protection information. In addition to the requirements outlined herein, you may be subject to local data protection laws, clinical trial regulations or local ethics requirements. Therefore, prior to uploading any data, it is advised that you speak with your data protection officer (DPO) to understand the full scope of your requirements. <i class="fab fa-creative-commons-pd-alt"></i> For public domain data, go to Step 4.


#### Step 1 - Data Sharing Agreement ####

Make sure that the upload and sharing of the dataset(s) in question is covered by the ImmUniverse consortium multi-party data sharing agreement known as the Memorandum of Understanding (`MoU`). Each partner in ImmUniverse has been asked to complete the `MoU`, so please check with your institution’s lead member to confirm.

If the dataset is not covered by the `MoU`, a separate agreement covering articles 26 & 28 needs to be signed between at least the UL and the uploading partner.

Before uploading any data, please speak with your DPO to ensure they are kept aware of, and agree with, the procedure for uploading your data as outlined in the Data Upload Instructions section.

#### Step 2 - For third party cohorts only ####

If you are applying to a third-party institution for access to their data, to then upload into the ImmUniverse Platform, please observe the following steps:

- Review 3d party agreement(s) (e.g., data access agreement (DAA)) with your institutional DPO
- If you have any additional questions about clauses in this agreement, in particular, how these clauses could affect the ImmUniverse project including data sharing with other partners, contact [UniVie]({% link ImmUniverse/contact.md %}) for support.
- Sign the agreements required by the third party to receive their data.

Guidelines for applying for Third party cohort data access are detailed [here]({% link ImmUniverse/data-access/3rd-party-cohort.md %}), with supporting material to ensure you have all of the information that you need in order to successfully apply for the access and ensure you can manage the complexities of dealing with partners outside of the consortium.
{: .bg-grey-lt-100 .p-2 .fs-3}

#### Step 3 - UL Data Protection Information Sheet or Appendix B ####

[Download]({{ site.baseurl }}/assets/docs/UL_Extended_Data_Information_Sheet-8.xlsx) and fill out the UL Data protection Information SHeet (`DISH`) or `Appendix B`.  This sheet will allow you to document use restrictions on the cohort data you're uploading.  Make sure that you document in this sheet the list of parties that should not receive access to the data or for whom access restrictions need to be implemented.

[Download DISH template]({{ site.baseurl }}/assets/docs/UL_Extended_Data_Information_Sheet-8.xlsx){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

#### Step 4 - Create a Ticket to access the data upload link ####

In order to manage the system efficiently we have created a ticketing system, [click here to create a ticket](https://ImmUniverse-redcap.lcsb.uni.lu/redcap/surveys/?s=39DXWAEEP8){:target='new'} for your secure data upload link. You will need to provide the following details in the ticket creation form.

- Institution or the site of recruitment and cohort name
- Name and email of the uploader
- Type of dataset and restrictions
- Short description of the data to be uploaded

You will also need to upload one or more documents, so ensure you have these handy.

- Upload filled and signed Data protection Information SHeet (`DISH`) or `Appendix B`
- Upload if any other legal document(s) are required

Then submit the form, and you will receive a confirmation of the ticket submission.

[Create a Ticket](https://ImmUniverse-redcap.lcsb.uni.lu/redcap/surveys/?s=39DXWAEEP8){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

#### Step 5 - Verification and data upload link creation ####

UL will manually verify the information and send you an email with a data upload link (from a platform called `DUMA`) and instructions on how to upload the data. Please store the information in a secure place.

With this email, you can transfer the data to the UL data platform via the secure `DUMA` link as described in the following section.

However, you should only upload data formatted according to the Section 2.

### Data Upload Instructions through DUMA link using Aspera client

The DUMA data upload email contains the DUMA upload link and a password link. Please follow the following steps to start data upload.

```mermaid

flowchart LR
  subgraph email[fa:fa-email<br />DUMA Link email]
    uplink[fa:fa-link DUMA<br />upload link]
    pass[fa:fa-link DUMA<br />password link]
  end
  copy[fa:fa-chrome fa:fa-firefox <br />Copy<br />password]
  pass --> copy
  open[fa:fa-chrome fa:fa-firefox <br />Open<br />upload<br />link]
  copy --> open
  uplink --> open
  aspera[fa:fa-apple fa:fa-windows<br />Install<br />Aspera<br />client]
  open --> aspera
  aspera --> Upload[fa:fa-upload<br />Upload<br />data]
  open -.-> linux[fa:fa-linux<br />Instruction<br />for Linux<br />platforms]

```

- Open the password link first, copy the password
- Now open the upload link, and use the password to access the AsperaWEB service

##### For Mac and Windows platforms
- Install Aspera client when prompted (if you are first time user)
- Install browser extension when prompted
- Restart the browser, and open the upload link again
- Now Aspera client will start automatically, allow it to run
- Use upload folder button to upload the folder containing your prepared data
- A graphical window will show the progress of the upload

##### For Linux platforms
- Click on the Help button in the DUMA upload link (top right corner)
- Follow the instructions in the Help to install Aspera Linux client
- Use the command line (CLI) to set parameters, and upload the prepared data folder


Aspera requires outbound connections from UDP and TCP port 33001, in strict firewall configurations you may need to request a change from your IT department ([Configuring the Firewall](http://download.asperasoft.com/download/docs/p2p/3.5.1/p2p_admin_win/webhelp/dita/configuring_the_firewall.html)). Detailed instructions can be found online at [AsperaWEB Quick Guide](https://howto.lcsb.uni.lu/external/exchange-channels/asperaweb/).
{: .bg-grey-lt-100 .p-2 .fs-3}
