---
layout: default
title: Experimental Metadata
parent: Metadata Upload Guide
grand_parent: Data Upload
nav_order: 2
---

# Experimental metadata (including multi-omics)
{: .no_toc}

ImmUniverse-WP4 activity includes the development and implementation of
analytical pipelines in order to preprocess the different types of
experimental data made available by the cohorts’ owners. To
efficiently develop the pipelines according to the specific omics, some
specific information needs to be provided.

Metadata about the experiment or assay needs to be provided per sample, and should be uploaded together with the actual experimental data (see also [Data Upload Guide]({{ site.baseurl }}/data-upload-guide/)) as tsv (tab-separated values) or Excel file. The required fields are described in details below.





## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


{: .no_toc .d-inline-block}
coming soon
{: .label .label-yellow }