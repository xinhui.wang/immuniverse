---
layout: default
title: Data Access
nav_order: 3
has_children: true
permalink: /data-access
---

# Data Access
How to access the ImmUniverse data?
1. Check if your institution has already signed the ImmUniverse MoU or not. If not, please contact local Data Protection Office or contact EURICE Team. 
2. If yes, create a LUMS account. [Create ticket](https://immuniverse-redcap.lcsb.uni.lu/redcap/surveys/?s=D9334AF8FW){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }{:target="new"}
3. Login to the Cohort/study. 
4. Find you data and download the data. 
</style>
