---
layout: default
title: Contact
nav_order: 6
has_children: false
permalink: /contact
---
# Contact information

## ImmUniverse Data Platform Team

### General help and support related to data upload and access

<img src="https://docs.google.com/drawings/d/e/2PACX-1vT-TwdPv5m7jnQ6mqNINWBApDhsA1oOphS9MjOArMOnQCmSyC8rrbEf3Hv5cvSH1G9ZLnLfthP45gQf/pub?w=50&amp;h=50"><br />
Venkata Satagopam<br />
<venkata.satagopam@uni.lu>

### Technology and data management
Xinhui Wang<br />
<xinhui.wang@uni.lu>

### Data Protection Information Sheet and DUMA/Aspera questions:
- Pinar Alper <datasteward@elixir-luxembourg.org>

## Data format and glossary

#### Clinical data:
- Nils Christian <nils.christian@ittm-solutions.com>

#### Virtual Biobank / Sample metadata /Omics data:
- Xinhui Wang <xinhui.wang@uni.lu>


