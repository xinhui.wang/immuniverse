---
layout: default
title: ImmUniverse IDs
nav_order: 4
has_children: false
permalink: /ImmUniverse-id
---

# ImmUniverse IDs
{: .no_toc}

To manage the complex data landscape of the ImmUniverse project, a consortium wide identifier re-pseudonymization scheme is developed, which uniquely tags every study participant, each collected biospecimen, and its assay by `ImmUniverse_SUBJECT_ID`, `ImmUniverse_SAMPLE_ID`, and `ImmUniverse_EXPERIMENT_ID` respectively. 

## Table of contents
#{: .no_toc .text-delta }

1. TOC
{:toc}

### Overview of the ImmUniverse ID assignment

- Each **study participant** in ImmUniverse is tagged with a unique `ImmUniverse_SUBJECT_ID`
- Each **collected biospecimen** is tagged with a unique `ImmUniverse_SAMPLE_ID` which contains the `ImmUniverse_SUBJECT_ID` to connect it to the participant
- Each **assay or experiment** done a biospecimen is tagged with a unique `ImmUniverse_EXPERIMENT_ID` which contains the `ImmUniverse_SAMPLE_ID` connecting it to the biospecimen as well as the participant
{: .bg-grey-lt-000 .p-6}

The structure of ImmUniverse IDs is illustrated with an example in Figure 1.

##### Figure 1: Structure of ImmUniverse IDs
{: .no_toc}

<img src="./assets/images/id_structure.png">

In the ImmUniverse Data Platform, it is _by design_ mandatory to tag each study participant with `ImmUniverse_SUBJECT_ID`; each biospecimen with `ImmUniverse_SAMPLE_ID`; and each experimental procedure or output with `ImmUniverse_EXPERIMENT_ID`.

ImmUniverse data-uploders are responsible to generate ImmUniverse IDs at their location before uploading the data to UL, and they will store a map between the ImmUniverse IDs and their internal IDs. The uploaded data should only contain the ImmUniverse IDs, not the internal IDs.
{: .bg-grey-lt-000 .p-6}

### ImmUniverse_SUBJECT_ID - unique subject identifier

Every study participant in the ImmUniverse consortium is assigned with a unique `ImmUniverse_SUBJECT_ID`. It should be generated at the data-uploaders location by re-pseudonymization following a common structure.

`ImmUniverse_SUBJECT_ID` consists of four parts - `PROJECT_NAME` , `COHORT_NAME` `SITE_OF_RECRUITMENT`, and `SUBJECT_NO` delimited by `_` (underscore) (example: `ImmUniverse_UC_UNILU_000001`)

- `PROJECT_NAME` - name of the project (always `ImmUniv`)
- `COHORT_NAME` - a the short name or acronym of the cohort (example: `UC`)
- `SITE_OF_RECRUITMENT` - the institute code of the site of recruitment or cohort custodian (example: `UNILU`).
- `SUBJECT_NO` - an **six-digit** re-pseudonymized subject identifier (example: `000001`). `SUBJECT_NO` is not required to be sequential.

For example, the subject identifier `ImmUniv_UC_UNILU_000001` refers to the study participant in the `ImmUniverse` project who was recruited at `UNILU` 
for the cohort `UC`. 
Note: UNILU does not provide any sample and this is an example.
### ImmUniverse_SAMPLE_ID - unique sample identifier

Each collected biospecimen or sample in ImmUniverse requires a re-pseudonymized `ImmUniv_SAMPLE_ID` which extends the `ImmUnive_SUBJECT_ID` with `TIMEPOINT`, `SAMPLE_TYPE`, "STUDY_TYPE"and `OMICS_TYPE` delimited by `_` (underscore).

- `TIMEPOINT` - Cohort or study specific identifier for visit(s) in longitudinal study

Example: `00`,`01` for longitudinal study; `NN` for non-longitudinal study
{: .pl-6 .fs-3}

- `SAMPLE_TYPE` - Type of the specimen - a code from a controlled vocabulary list (Table 1)
- `STUDY_TYPE` - Type of the study in ImmUniverse- a code from a controlled vocabulary list (Table 1)
- `OMICS_TYPE` - Type of the Omics data - a code from a controlled vocabulary list (Table 1)

Example: `01`,`02` for sampling repetitions; `NN` if there is no sampling repetitions
{: .pl-6 .fs-3}

For example, the sample identifier `ImmUniv_UC_UNILU_000001_DPP_BLOOD_001_01` refers to the alliquote `001` of the blood sample collected at the time-point `01` from the subject `ImmUniverse_UC_UNILU_000001` in DPP study.


##### Table 1: SAMPLE_TYPE codes and description
{: .no_toc}

<div  class="bg-grey-lt-000 p-6">
<div style="--aspect-ratio: 16/9;">
<iframe src="https://docs.google.com/spreadsheets/d/1K6ubcj0248uyL7VXdGpEO39OvtDuStt6SCBvw2pZ2Xk/edit#gid=1178873996" style="border:0;margin:auto"></iframe>
</div>

</div>



Please contact the Data Platform Team if you need help with renaming or formatting your high-dimensional data files.

### More help

Do you need more help to assign ImmUniverse IDs to your data? Please contact [Data Platform Team](/contact).
