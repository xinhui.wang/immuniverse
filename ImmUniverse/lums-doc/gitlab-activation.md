---
layout: default
title: GitLab Activation
parent: LUMS Account
nav_order: 4
---

# GitLab Activation

1. Go to R<sup>3</sup>-Gitlab server [link](https://git-r3lab.uni.lu/imi-ImmUniverse/consortium).
2. Login with your <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span> Account.
3. Now, your <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span> user-id is added to R<sup>3</sup>-Gitlab, but it still requires manual authorisation from UL to access ImmUniverse repositories. Wait for the authorisation from UL.
4. :mega: You will be notified by email once the authorisation is done.
5. Login with your <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span> account again, and now you can access ImmUniverse repositories.

>  Please note, your first-time login (Step 1 and 2) is essential to add your <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span> user-id to R<sup>3</sup> Gitlab server. Thus you cannot be authorised for accessing ImmUniverse repositories if you have not done it.


```mermaid
graph TD
    Start --> id1(Go to R3 Gitlab server) --> id2[Login with LUMS]
    id2 --> id3[First time Login] --> id4[R3-Gitlab activation]
    id4 --> id5[UL authorization] --> id6(Access ImmUniverse repositories)
    id2 --> id2a[Login after authorization] --> id6
```
