---
layout: default
title: LUMS Account Onboarding
parent: LUMS Account
nav_order: 2
---

# LUMS Account Onboarding

1. If you have already submitted a ImmUniverse <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span> account related request please go to Step 2. Or you have not done so, please submit a new request through this [form](https://ImmUniverse-redcap.lcsb.uni.lu/redcap/surveys/?s=D9334AF8FW).
2. For new account related request, you will receive an email from `UL System Admins` usually within 1 working day. The email will contain a link to activate your new <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span> account and change your password. Please check your spam/junk inbox too.
3. For addition of new services to your existing account, you will receive an email when it is done.
4. You can now access OwnCloud, tranSMART, and ADA services with your <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span> Account, if you have applied for them. But for REDCap and R<sup>3</sup> Gitlab services, additional activation steps are required.
