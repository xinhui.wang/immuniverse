---
layout: default
title: REDCap Activation
parent: LUMS Account
nav_order: 3
---

# REDCap Activation

1. Go to ImmUniverse-REDCap server [link](https://ImmUniverse-redcap.lcsb.uni.lu).
2. Login with your LUMS Account.
3. Fill out the details like your name and email address.
4. :mega: Check email for the confirmation link to verify your email address.
5. Click on the link to verify your email address. Now, your LUMS user-id is added to REDCap, but it still requires manual authorization from UL to access ImmUniverse cohorts.
6. Wait for manual authorization from UL to add your account to access group
7. :mega: Check email for authorization confirmation
8. Login and access ImmUniverse cohorts

>  Please note, your first-time login (Step 1 and 2) is essential to add your LUMS user-id to REDCap server. We cannot authorize your LUMS user-id for accessing ImmUniverse cohorts if you have not done it.

```mermaid
graph TD
    Start --> id1(Go to ImmUniverse-REDCap server) --> id2[Login with LUMS]
    id2 --> id3[First time Login] --> id4[Fill out the basic details and submit]
    id4 --> id5[Check your email for REDCap email verification] --> id6(Click on the link to verify your account) --> id8[Wait for UL Authorization Confirmation Email] --> id8a[Login with LUMS]
    id8a --> id9[Access ImmUniverse cohorts]
    id2 --> id2a[Login after authorization] --> id9
```
