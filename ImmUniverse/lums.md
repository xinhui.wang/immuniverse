---
layout: default
title: LUMS Account
nav_order: 1
has_children: true
permalink: /lums
---

# ImmUniverse-LUMS account

<span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span>is LCSB's User Management System. ImmUniverse partners need LUMS account for accessing the ImmUniverse Data Platform services including the cohort catalog, REDCap, GitLab, Ada and tranSMART.

To apply for a new LUMS account, create a ticket using the following link.

[Create ticket](https://immuniverse-redcap.lcsb.uni.lu/redcap/surveys/?s=D9334AF8FW){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }{:target="new"}

Account holders can use the same link for their account reactivation or password change request.
