---
layout: default
title: Biosample Metadata
nav_order: 3
has_children: false
permalink: /biobank
parent: Metadata Upload Guide
grand_parent: Data Upload
---

# Biosample Metadata in ImmUniverse Virtual Biobank
{: .no_toc }

![image alt >]({{ site.baseurl }}/assets/images/data-upload-illus.png) Objective: Develop a common standard in the ImmUniverse Consortium for biobanks’ biosample summary data, collectively known as biosample-metadata, create catalog solution for biosample-metadata and develop a generic workflow for biosample request.


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Background {#background-1}

Biobank collects and stores biosamples and associated metadata from patients or participants in a cohort study, e.g. blood, tissue, or stool. Biobanks can be in a centralised location for long-term population studies, or in decentralised collections of patient biosamples stored by individual research groups in multi-site clinical studies like ImmUniverse.

Due to decentralised organization of individual biobanks, a major challenge for researchers is to find and locate biosamples at other centers. ImmUniverse Consortium’s `WP4.3` aims to create a responsive biosample-metadata-catalogue or virtual-biobank of multi-dimensional biosamples (archived and newly obtained) with aligned clinical data. This can be achieved by building a catalogue of biobanking biosample metadata, defining a common standard for such metadata, which facilitates exchange between researchers in ImmUniverse consortium, and developing a workflow that enables researchers to request and retrieve biosamples and associated data if exists.



## Task

**Task 4.3: Establish the virtual biobank platform and sample curation..**

This task aims to curate, align and harmonise biospecimens collected as part of standard clinical care and clinical trials. We will develop a web application accessible via the data and analysis portal, to catalogue sample metadata together with a unique ImmUniverse identifier (generated in line with TCGA guidelines, https://docs.gdc.cancer.gov/Encyclopedia/pages/TCGA_Barcode/). This tool will be used for consortium partners to view and request biospecimens as required, to track sample shipments and use amongst participating sites, and to log supplementary samples (D4.3). In addition to sample type, volume/size, collected variables will relate to participants (demographics, clinical phenotype, interventions), sampling (visit number, timing, method used), processing and storage (how, where including principle investigator details, governance relating to access), and re-callability of source participants. We will also collect the Quality Control (QC) data in collaboration with WP2 (Task 2.2). A data template with pre-defined variables will be used to collect and then import these data into the web-application using ETL (Extract-Transform-Load) script.(~~D1.1~~D1.3).
{: .fs-3}

Partners involved: UNILU*, ITTM, HUNIMED, CAU Duration: month 1 ? 60.


## Method

To generate a common data model for storing biosample metadata and biobanking details in ImmUniverse project, UL and a collaborating group of biosample providers have developed a Virtual Biobank Template. This template needs to discussed in the consortium and based on the feedback a finalised version of Virtual Biobank Template will be released.

```mermaid

graph LR
    id0((UL)) --> id1(Virtual Biobank Template) --> id2((ImmUniverse Consortium))
    id2 --> id3(Finalised Virtual Biobank Template)
    id2 --> id0((UL))
    style id3 fill:#F28F89

```
<i class="far fa-map"></i> The current version of the template can be accessed [here](https://docs.google.com/spreadsheets/d/1unuJo6z-KffxaK6fRtMJOrANS2V25Y9XdYeu4LvCbFE/edit#gid=250235958){:target='new'}. ImmUniverse data uploaders or biobank managers can comment on the template.
{: .bg-grey-lt-000 .p-6 .fs-3}

Once the final Virtual Biobank Template is released, the data-uploaders can use the template to batch upload the biosample bionbanking metadata to the Virtual Biobank REDCap EDC.

```mermaid

graph LR
    id0(Biosample<br />Providers<br />Data<br />Uploaders) --> id1[Formatting &<br />ImmUniverse ID<br />tagging]
    id2(Finalised<br />Virtual<br />Biobank<br />Template) --> id3(CSV/TSV for<br />batch upload)
    id1 --> id4(Data<br />formatted<br />in CSV/TSV)
    id3 --> id4
    id4 --> id5[(Batch Upload<br /> to Virtual<br />Biobank<br />REDCap EDC)]
    id5 --> id6([Curation])
    id6 --> id8(Ada<br />Virtual Biobank)
    id6 -.-> id7([Format<br />Correction]) -.-> id4
    style id2 fill:#F28F89
    style id8 fill:#FFE661

```



## Virtual Biobank Template

Virtual Biobank Template has two sets of variables:

1. **Common minimum biosample metadata** - required information about the biosample to describe where and from whom the biosample was collected and to generate `ImmUniverse_SAMPLE_ID` for each biosample
2. **Sample biobanking metadata** - Metadata describing the physical extraction and existence of the biosample like location, collection methods, anatomical sites, biosample storage

### Common minimum biosample metadata

A set of common minimum biosample metadata is required for any biosample irrespective of whether their biobanking information is available or not. Each biosample in ImmUniverse Data Platform is tagged with a unique identifier `ImmUniverse_SAMPLE_ID` which is generated from this information.

##### Table 1: Common minimum biosample metadata variables and description
{: .no_toc}
<div style="--aspect-ratio: 16/9;">
<iframe src="./assets/images/id_structure.png"></iframe>
</div>



### Biosample biobanking metadata



