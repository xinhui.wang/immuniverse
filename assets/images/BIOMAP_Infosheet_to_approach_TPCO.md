

____
This document aims to help a cohort representative to reach out to their third-party cohort owner to start the data access ball rolling. Please feel free to use whichever part of this document is helpful.
____

# BIOMAP Info sheet for cohort representatives
![](Fig1_Infosheet.jpg)

The European research project BIOMAP (Biomarkers in Atopic Dermatitis and Psoriasis) is the first large-scale multinational public-private partnership on chronic inflammatory skin diseases. This five-year project will address key unmet needs in understanding atopic dermatitis and psoriasis by analysing data from more than 50,000 patients, and have a broad impact on improving disease understanding, patient care and development of future therapies (www.biomap-imi.eu).

The team comprises 26 academic and five industry partners as well as five patient organisations from 12 countries.

## General Description: Aims and Objectives

BIOMAP aims to build on existing data to establish a knowledge base on the pathogenesis, molecular characteristics and natural history of atopic dermatitis (AD) and Psoriasis (Pso) in order to further develop and define disease endotypes. Effort will be focused around the following three main themes

* Identification of major determinants of manifestation, progression and comorbidity development;
* Improved understanding of shared and distinct disease mechanism(s) and associated signatures, and their relative importance in patient subpopulations; and
* Identification of objective markers capable of assessing disease-related individual patient trajectories.

All these findings will shift the current diagnosis and management paradigm, from largely reactive to **proactive patient-centred** strategies and to maximize the impact of this project the results will be published rapidly in key journals. It is further anticipated that findings will support the definition of endotypes relating to these key outcomes and stimulate future research on mechanisms and innovative treatments directed at endotype components. The specific objectives of BIOMAP that relate to this application are:

* To build a **Data Warehouse**, collecting and organizing clinical, experimental and multi-omics data from large high-quality patient collections, disease registries, epidemiological cohorts and industry trials. A Data Analysis Portal will enable immediate access to the Data Warehouse, facilitating multi-user analysis to investigate interactions between clinical features and molecular pathways. These analyses will aid definition of disease endotypes and associated molecular signatures. The Data Warehouse is fully GDPR compliant and adheres to the highest standards of data protection and security - see Data protection / Legal framework as provided by leading experts in this area (Prof. Nikolaus Forgó, University Vienna; Prof. Reinhard Schneider, University of Luxembourg).
* To identify influential life events and environmental factors and to generate predictive models and biomarkers of relevant disease outcomes (e.g., severity, progression, comorbidity development, stratification and patient management).
* To cross-reference findings from AD and Pso to look at similarities and differences that could be used to refine endotypes or inform selection of targets for therapy; and
* To publish BIOMAP findings with maximum impact in appropriate journals and present them at conferences with reference to the contributors like yourselves (see for details: Publication Process).

High quality datasets from large-scale population-based studies (*such as ALSPAC, UK BIOBANK, KORA, POPGEN, Generation Scotland*) and disease-focused registries and cohorts (such as TREAT, A*Star TWINS UK) will be included. The [*Please add name of the cohort you are approaching here*] cohort, with its *deeply phenotyped, multi-omic data* is thus recognized as a high value, exemplar cohort to complement and synergise with existing multi-omic *psoriasis/atopic dermatitis datasets* (*such as P2N, MAARS, BIODIP* as well as data from pharma trials) for this programme.

## Benefits to cohort owners

BIOMAP will change the way we manage these diseases, and your patient data are extremely important to the work of the consortium – therefore we would like to extend an invitation to you to contribute your data in return for the following:
(i)	We will make sure that you contribution is appropriately recognized in this patient centric effort by e.g. making sure that your cohort is highlighted on our [BIOMAP website](https://biomap-imi.eu/).
(ii)	With regards to publications - BIOMAP results will be disseminated through journal articles as well as talks/posters at conferences. Whenever such results were obtained with the help of your cohort data this will be appropriately acknowledged, for example, by providing co-**authorship** and naming your cohort along with the study logo (subject to concordance with ICHJE guidelines, and the BIOMAP Author and Publication Checklist). Furthermore, you will receive an electronic copy of any theses that use your data as soon as possible after a degree is awarded.
(iii)	**Data access costs** (if mandatory): Depending on the amount and type of data, costs for data access and provision will be negotiated on a case-by-case basis.

The datasets requested from [*Name of the cohort*] primarily relate to work carried out in WP [*1, 2, 3, 4, 5, 6*], as *defined in the attached research protocol and related timelines*.

A key opportunity in the BIOMAP consortium is pooling of data to achieve greater analytical power, both through meta-analyses of individual datasets and through pooling (where possible) patient level data for re-analysis. It is not the intention of BIOMAP to duplicate analyses already completed/planned within [*Name of the cohort*] or to publish data from [*Name of the cohort*] ahead of the [*Name of the cohort*] primary publications. There are robust systems in place to ensure this is avoided.
We will not be generating any new data as we are not requesting samples in this application.
To ensure that none of the planned analyses duplicate those you may have planned in [*Name of the cohort*] the following provisions are in place (as they are for all datasets contributing to BIOMAP):
*	As the BIOMAP partner applying for use of [*Name of the cohort*] data on behalf of BIOMAP, [*Name applicant*] will become the *data controller*.
*	As data controller, [*Name applicant*] will automatically be notified of any application from any partner to use [*Name of the cohort*] data and will be responsible for ensuring use of the data is for the specified purpose agreed by [*Name of the cohort*].

## Public Impact

### How will your research be undertaken within public interest?
BIOMAP aims to improve the lives of patients affected by the two most common inflammatory skin conditions. Addressing key unmet needs in treating atopic dermatitis and psoriasis by analysing data from more than 50 000 patients, the five-year project will have a broad impact on disease understanding, patient care and future therapies. The team comprises 26 academic and five industry partners as well as five patient organisations.

**Short lay summary**
The lives of patients affected by atopic dermatitis and psoriasis could be improved thanks to the start of an EU-funded research project BIOMAP (Biomarkers in Atopic Dermatitis and Psoriasis). The five-year project will address key unmet needs in treating these common inflammatory skin conditions by analysing data from more than 50 000 patients to improve disease understanding, patient care and future therapies. The clinicians and scientists of BIOMAP, who have joined forces in a large public-private partnership, will examine the causes and mechanisms of these conditions. By analysing the largest collection of patient data ever and performing advanced molecular investigations at the single cell level and in the tissue context, they aim at identifying biomarkers for variations in disease outcome. Taking advantage of recent technical developments in translational medicine, the project will drive drug discovery and improve direct disease management by combining clinical, genetic and epidemiological expertise with modern molecular analysis techniques and newly-developed tools in bioinformatics.

## Further Details about BIOMAP

### Background
Current classification criteria in AD and Psoriasis largely rely on the combination of clinical characteristics, which are not standardised and not stable over time. They thus represent rather crude measures of phenotypes that, so far, do not include deep and long-term clinical phenotypes (recently shown in atopic diseases) and provide only a very limited perspective on the heterogeneous mix of pathobiologically distinct mechanisms, and therefore fail to define actionable disease subtypes. With the advent of new technologies that enable an increasing molecular resolution on all OMICs-levels along with advanced data mining and mathematical modelling approaches, the development of criteria for defining endotypes that are based on disease mechanisms and molecular pathways is now a key focus within the scientific community. The Innovative Medicines Initiative 2 topic call Genome-Environment Interactions in Inflammatory Skin Disease was developed in response to this opportunity and Prof. Stephan Weidinger (Christian-Albrechts-Universität zu Kiel) and Dr Paul Bryce (Sanofi) successfully led an EU wide consortium bid. As part of the programme of work, the value of the [*Name of the cohort*] cohort data set was recognized, and an agreement in principle to collaborate was generously provided by Prof./Dr *XXXX* , member of the [*XXXX*] consortium.

### Research Plan

![](Fig2_Infosheet.jpg)

The research plan for BIOMAP is divided into six scientific work packages (WP):

**WP1 Clinical Research Network, Epidemiology and Genetics**
WP1, working seamlessly with WP2, will provide accurately annotated and harmonised clinical data and biospecimen resources. A detailed mapping exercise of the cohorts underpinning BIOMAP will facilitate comparisons and data pooling, and identify critical gaps to address. This resource, together with SOPs for collection, description and integration of existing and newly obtained data, will be accessible to all partners via the data and analysis portal (WP2). The multi-disciplinary clinical research network will facilitate supplementary sample collection, and work closely with patients and relevant organisations to ensure that the focus (defining outcomes of importance) and outputs from BIOMAP benefit patients and society. WP1 will also interrogate large longitudinal datasets (leveraging complementary data from population studies and registries) to systematically define and validate endotypes, refine outcomes, and identify predictors. Tasks will be harmonised and aligned across AD and Pso to allow cross-disease comparisons, and will encompass both adult and paediatric populations.
Contact persons: Catherine Smith (Catherine.Smith@kcl.ac.uk), Witte Koopmann (WEKDK@leo-pharma.com), ‘Matladi Ndlovu (Matladi.Ndlovu@ucb.com)

**WP2 Bioinformatics and Data Modeling Core**
The main objectives of WP2 are: i) to provide the infrastructure and tools for optimised, time efficient analysis across the consortium within the requisite governance and regulatory framework, capitalising on analytic expertise/pipelines throughout the consortium; and ii) to further develop and refine disease endotypes and candidate biomarkers based on outputs from WP1/3/4/5 using cutting edge integrative data modelling. Core Bioinformatics activities (apart from modelling) will also take place in WPs 3, 4, and 5. The work in WP2 will be carried out in the context of five main tasks, internally divided into data management and software implementation tasks (WP2A), and data analytics and modelling (WP2B).
Contact persons: Dario Greco (dario.greco@tuni.fi), Reinhard Schneider (reinhard.schneider@uni.lu), Emanuele de Rinaldis (Emanuele.deRinaldis@sanofi.com), Phil Scordis (phil.scordis@ucb.com)

**WP3 Tissue-based Signatures and Pathways**
This WP will use existing and incoming molecular data from blood and skin samples to build knowledge representations of Psoriasis and Atopic Dermatitis and their outcomes (as defined by WP1) in the form of individual molecular signatures, pathways and networks. Further, we will search for molecularly defined endotypes, and how these might associate with clinical outcomes. These approaches will seek to drive the identification of signatures specific or shared across endotypes, and for investigating underlying causal mechanisms. Longitudinal analyses will also be applied where possible to differentiate between longitudinally-stable “core” endotype and dynamic disease-activity related signatures. Finally, we will develop knowledge-based both machine- and human-readable comprehensive representations of disease mechanisms (“disease maps”).
Contact persons: Stephan Weidinger (sweidinger@dermatology.uni-kiel.de), Witte Koopmann (WEKDK@leo-pharma.com), Karen Page (karen.page@pfizer.com)

**WP4 Host-Microbiome Interaction**
The observation that microbial factors are important triggers of AD and Pso – and might even be among the main causative factors of AD manifestation in genetically susceptible individuals – indicates potential for diagnostic and therapeutic exploitation. WP4 will investigate skin and gut microbial patterns linked to AD or Pso, and potential disease subtypes based on microbial heterogeneity. Furthermore, normal variability across time scales due to disease and disease-activity related changes, and host molecular constituents related to normal and pathological shifts will be dissected.
Contact person: Harri Alenius (Harri.Alenius@ki.se)

**WP5 Immune cell Phenotypes and Signatures**
Mass cytometry allows the comprehensive analysis of the composition of the immune system both at the single cell level and in the tissue context, allowing the identification of disease-associated immune subsets. Here the simultaneous analysis of the adaptive and innate immune system reveals potential cross-talk between disease-associated responses in these immune compartments. In combination with single cell sequencing and epigenetic profiling, this provides unprecedented insight into the nature and polarization state of such disease-associated immune subsets; this information can be used for the design of minimal antibody panels for specific isolation of selected cell subsets for further functional analysis and the determination of disease-associated T and B cell receptor repertoires. We will take advantage of our recent work in which we have used such a combination of cutting-edge approaches to gain insights into the nature and mode of action of (disease-associated) immune subsets.
Contact persons: Frits Koning (f.koning@lumc.nl), Martin Hodge (Martin.hodge@pfizer.com), Adnan Khan (Adnan.Khan@ucb.com)

**WP6 Biomarker Characterization and Verification**
WP6 will verify and assess biomarkers in AD and Pso, as well as implement a blueprint for future clinical utility of these biomarkers through close interaction with regulatory authorities, the clinical research network (WP1), and other stakeholders such as patient organisations. Initially, a systematic literature and database mapping will be carried out to summarise the existing evidence in biomarker research in Pso and AD. Subsequently, existing biomarkers and newly detected marker sets from WPs 1-5 will be incorporated and prioritised for their capacity to discriminate disease endotypes and to predict relevant outcomes using in silico and in vitro approaches. Finally, WP6 will establish a pipeline for functional follow-up (analytic validation) and implementation of biomarkers identified into clinical trials (clinical validation).
Contact persons: Kilian Eyerich (kilian.eyerich@tum.de), Joe Rastrick (Joe.Rastrick@ucb.com)

## Details about the Legal Framework incl. Publication Process

### Data protection and integrity
University of Luxembourg (UNILU), a member of the BIOMAP consortium, has developed a secure and modern data and computing platform to store, process, and access various types of data collected or to be collected in the BIOMAP project. As Human cohort data is sensitive in nature, UNILU accepts only pseudonymized or anonymized data from Data Providers. Transfer of data occurs over an end-to-end encrypted, high-speed channel, called LCSB Data Upload Manager (DUMA) and built on IBM's Aspera Technology. UNILU has a defined policy for encryption of sensitive data, working disk, and backups for data at rest. The Computing platform consists of servers and storage devices that are hosted in a highly secure Tier 3 data centre with regular backups, controlled access and video surveillance. Physical access to any data center resource requires multiple approval steps. Digital access is limited to authorized users and a restricted number of UNILU System Administrators. Operating system and application authentications are required for any access and all activities are logged. User roles and access levels are defined depending on the legal agreements at both consortium and individual levels. For each dataset, a list of users who have access to the dataset is defined and maintained by BIOMAP coordinating team together with UNILU. Each person can only access the data to which they have a right of access. A data backup concept is defined and implemented. Backup of the Raw data is present on a different server with flood and fire protection.
Temporary copies of the data are permanently erased from any storage device before recycling. Internet-facing systems are regularly tested against vulnerabilities in accordance with the UNILU's Information Management Security Policy and in collaboration with the Chief Information Security Officer. An IT Code of Conduct in accordance to IT Security and Privacy policy is defined and implemented internally. UNILU has an assigned Data Protection Officer (DPO) and an online system to record the access rights and legal agreements for any dataset hosted in the data and computing platform. Further, we have an established procedure to record and report data breaches to the national authority. UNILU regularly conducts the IT risk assessments, and mandatory data protection trainings for all employees working with sensitive data. It has strict zero tolerance policy in case of probable data breach or threat of such.

### Legal Framework
Within BIOMAP the following legal documents are in place:
i) **Grant Agreement** (defines the relation between the funding agency (IMI JU) and the BIOMAP coordinator / the BIOMAP consortium), ii) **Consortium Agreement** (regulates the relation between consortium partners (= beneficiaries)), iii) **Memorandum of Understanding (MoU)** addressing GDPR Article 26 and 28 and iv) **Confidentiality Agreement** concluded with external individuals (e.g. members of the Patient Board and Advisory Board).

The BIOMAP Consortium Agreement covers aspects like the internal organisation of the consortium; Intellectual Property Rights (IPR); additional rights and obligations on dissemination, use and access rights; the settlement of internal disputes; liability, indemnification and confidentiality arrangements between the beneficiaries.
The MoU describes the overall process of data transfer and data access, focusing specifically on the GDPR related requirements which may not be already covered by the BIOMAP Consortium Agreement. This MoU is vital for partner UNILU to host and to make any Cohort Data that might contain personal information of data subjects accessible to the BIOMAP partners. The MoU is required to be signed by all BIOMAP consortium partners that provide data and/or would like to access Cohort Data hosted by UNILU.
In addition, the BIOMAP project explicitly has, as one of its work packages, the monitoring and offering of ongoing advice and support regarding emerging ethical and/or regulatory issues (WP8). In particular, support will be provided to ensure that study-related documents and ethics approvals are appropriate for research activities. The current and emerging regulatory landscape will be scanned and advice on implementation of relevant changes will be given on an ongoing basis, to help ensure compliance with currently developing legal and regulatory norms in the wake of GDPR across Europe. The parties have (and are) concluding additional contracts, which will govern their respective responsibilities, inter alia, vis-a-vis data subjects.

### Publication process
All peer-reviewed publications must be released by the whole consortium specifically making sure that key contributions like yourselves are recognised in agreement with Clause 7.5 ‘Dissemination of Results’ of the BIOMAP Consortium Agreement:
*	Final manuscript draft should be circulated to other authors by written notice at least forty-five (45) days prior to intended manuscript submission date.
*	Any authors may suggest changes to the proposed manuscript within thirty (30) days of notification.
Authors will ensure that authorship accurately and adequately represents key contributors to the publication. With respect to BIOMAP this should be considered in the context of:
*	Sample and/or data acquisition;
*	Formulation of the research question, design, analysis and/or interpretation;
*	Cohort specific publication requirements as stipulated by cohort custodians ;
*	Individuals who substantially contributed to the intellectual content of the publication out with the above categories.

#### Source of funding
BIOMAP has received funding from the Innovative Medicines Initiative 2 Joint Undertaking (JU) under grant agreement No. 821511 and the participating pharmaceutical companies. The JU receives support from the European Union’s Horizon 2020 research and innovation programme and EFPIA.
#### Acknowledgement and Adherence to [*Name of the cohort*] Policies:
I confirm that, if data/samples are provided to me, I will adhere to all applicable human subject research and data protection laws and regulations and to the [*Name of the cohort*] Consortium’s publication policy. I will cover all relevant data access and management costs. For sample access, I will cover [*Name of the cohort*] sample identification and shipment costs. I will not pass data or samples on to third parties and will return any surplus materials to the [*Name of the cohort*] consortium.
 
## Draft email for approaching third-party cohort owners

Dear [*Name*],

In [*month/year*] you kindly agreed an in-principle commitment to collaborate on the EU-wide consortium bid ‘BIOMAP - Biomarkers in Atopic Dermatitis and Psoriasis’ (led by Stephan Weidinger, Christian-Albrechts-Universität zu Kiel, Catherine Smith, King’s College London, Witte Koopmann, LEO Pharma, and Paul Bryce, Sanofi) in response to the Innovative Medicines Initiative 2 topic call Genome-Environment Interactions in Inflammatory Skin Disease. The bid was successful and the consortium launched in April 2019 (see https://biomap-imi.eu/).

The overall objective of BIOMAP is to build on the existing knowledge around the pathogenesis, molecular characteristics and natural history of AD and Psoriasis to further develop and define disease endotypes. Efforts are focused around the following three main themes:

1. Identification of major determinants of manifestation, progression, comorbidity development and treatment response;
2. Improved understanding of shared and distinct disease mechanism(s) and associated signatures, and their relative importance in patient subpopulations; and
3. Identification of objective markers capable of assessing disease-related individual patient trajectories.

Fundamental to this programme is the BIOMAP Data Warehouse.  The Data Warehouse assimilates existing and incoming clinical, experimental and multi-omics data from patient collections, disease registries, epidemiological cohorts and industry trials, and then provides secure multi-user access to research around our themes.
We have now built the technical platform with rigorous measures in place to ensure compliance with the Data Protection Act and preservation of data integrity, and to ensure compliance with any stipulated requirements by data contributors with respect to access, use and publication of analyses.
As the named 'data custodian' for [*Name of the cohort*] I am now getting in touch to request formal approval for use of [*Name of the cohort*] BIOMAP and really hope you are still keen and able to collaborate with us on this research.

[*Please put here your individual “finish off” the email*]

With best regards

## Interactions to facilitate Data Acquisition from TPCO

____
Once a TPCO is informed with the above email and supplied with the background information as necessary, we will need to explore the process of bringing their data into the platform – where possible.  Here, as above, is some information to support the subsequent discussion:
____

Dear TPCO [*Name of the cohort*],
You have been contacted previously and identified as the main contact of the cohort [*Name of the cohort*], which you have agreed to integrate into the BIOMAP platform.
We would now like to collect detailed information about this cohort to:
1.	Help the BIOMAP glossary team to identify commonalities with other BIOMAP cohorts to define the BIOMAP glossary
2.	Get an understanding of the variables and data format to later map the data to the BIOMAP glossary and ultimately load it to the BIOMAP platform.

The BIOMAP glossary will be based, in part, on the principles of the OMOP common data model (https://www.ohdsi.org/data-standardization/the-common-data-model/), a model which was specifically designed for harmonisation of disparate databases, endorsed by the European IMI project EHDEN.
The process of data harmonised, once the glossary has been completed, is outlined below. The rules used to map your data to the BIOMAP glossary will be shared with you on completion.
To start this, we would like to ask you to send us your data dictionary.

**What should be included in the data dictionary**
The data dictionary (sometimes also called a codebook) describes the variables available for a cohort. It should include labels, detailed descriptions, the data type (numeric, text, date, …), allowed values, etc. A machine-readable version of your data dictionary (preferably as Excel or comma separated values (CSV) file) is required.
At a later stage, the variables of the data dictionary need to be matched to the BIOMAP glossary, which will allow us to harmonise the data and use it for analysis.

**How to create a new data dictionary**
An example of a data dictionary is attached. If you don’t have a machine-readable data dictionary available, you can start creating one using this template. The header cells in the template show detailed information (e.g how to fill
the respective column) when selected.

**How to work with an existing data dictionary**
If you have a data dictionary but you are not sure if the format is suitable, please get in touch with us before spending time on converting it, we may be able to automate part of the process. This is especially true if your data dictionary contains many variables.

Once we have reviewed the data-dictionary, below is the workflow we will follow in order to harmonise a cohort dataset.

1. Data Upload.
Cohort owner will upload data to the BIOMAP portal. Technical details can be found in the Data Upload Guide (Data upload guide, https://imi-biomap.pages.uni.lu/biomap-portal/data-upload-guide/). The upload process ensures that data is securely.

2. Data and Dictionary checking.
Data and Data Dictionary are checked by the harmonisation team for completeness. In case gaps are identified, these are communicated back to the cohort owner.

3. Q&A meetings.
Cohort owner and harmonisation team schedule teleconferences to clarify questions about data dictionary and data, and to discuss how variables should be mapped to the BIOMAP glossary. Mappings of variables (or combination of variables) towards the BIOMAP glossary will be based on these Q&A meetings.

4. Prepare mapping document.
The harmonisation team prepares the mapping document describing how cohort variables will be mapped towards the BIOMAP glossary, based on the outcome of the Q&A meetings.

5. Mapping review.
The cohort owner reviews the mapping document. If needed, a new cycle of steps 3 to 5 is scheduled.

6. Mapping implementation and data preparation.
The harmonisation team implements the extract, transform and load (ETL) pipeline based on the reviewed mapping document. This pipeline is used to populate the data portal by loading the data into the tranSMART data warehouse and preparing data archive files containing the harmonised data ready for download.

### Exceptions

We recognise that some TPCOs will be unable to supply their data to BIOMAP for integration in the central platform and we will endeavour to work with them and the BIOMAP representative to identify a solution, which may include facilitating the external development of harmonisation and analytical workflows but this will not be part of the standard workflow – below is an augmented direction to such TPCOs.
 
1. Data Upload.
Third party cohort owner to *provide the data dictionary and artificial data sample*. The requirements for the data dictionary are documented in the Metadata upload guide (https://imi-biomap.pages.uni.lu/biomap-portal/metadata-upload-guide/#data-dictionary).

The artificial sample data should not contain any personal data of real persons, but provide a representative sample of what the dataset contains. This information should be provided as Excel tables or tsv files (https://en.wikipedia.org/wiki/Tab-separated_values).

2. Data and dictionary checking.
Data and Data dictionary are checked by the harmonisation team for completeness. In case gaps are identified, these are communicated back to the cohort owner.

3. Q&A meetings.
Cohort owner and harmonisation team schedule teleconferences to clarify questions about data dictionary and data, and to discuss how variables should be mapped to the BIOMAP glossary. Mapping of variables (or combination of variables) towards the BIOMAP glossary will be based on these Q&A meetings.

4. Prepare mapping document.
The harmonisation team prepares the mapping document describing how cohort variables will be mapped towards the BIOMAP glossary, based on the outcome of the Q&A meetings.

5. Mapping review.
The cohort owner reviews the mapping document. If needed, a new cycle of steps 3 to 5 is scheduled.

6. Mapping implementation and data preparation.
The *cohort owner* implements the extract, transform and load (ETL) pipeline based on the reviewed mapping document. This pipeline is used to generate a local harmonised dataset that can later be used to run analysis as requested by the BIOMAP consortium.


 
