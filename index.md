---
layout: default
title: Home
nav_order: 0
description: "ImmUniverse Data Platform is developed to establish, populate, maintain and provide access to a robust and secure data management and analysis system through a software portal, based on the output of Task 4.1 (D4.4)."
permalink: /
---

# ImmUinverse Data Platform
{: .fs-9 .no_toc}

ImmUniverse data platform is developed to a web application accessible via the data and analysis portal, to catalogue sample metadata together with a unique ImmUniverse identifier (generated in line with TCGA guidelines, https://docs.gdc.cancer.gov/Encyclopedia/pages/TCGA_Barcode/). This tool will be used for consortium partners to view and request biospecimens as required, to track sample shipments and use amongst participating sites, and to log supplementary samples (D4.3). In addition to sample type, volume/size, collected variables will relate to participants (demographics, clinical phenotype, interventions), sampling (visit number, timing, method used), processing and storage (how, where including principle investigator details, governance relating to access), and re-callability of source participants. This port will establish, populate, maintain and provide access to a robust and secure data management and analysis system through a software portal, based on the output of Task 4.1 (D4.4). Harmonisation of existing clinical data will be achieved with the help of ImmUniverse glossary developed in Task 4.2, and a secured database will be populated with these data and with pre-processed omics data. The data portal will provide easy and robust interface for the input of new clinical and molecular data. Several analytical tools from existing initiatives such as I2B2, tranSMART, SmartR, Ada, EGA (European Genome-phenome Archive), DataSHIELD and eTRIKS platform will be made available. This page is developed to grant the account/access, links to the data collection and data storage, more tasks will be embed in this page.

{: .fs-6 .fw-300 }

```mermaid

flowchart LR
up1[fa:fa-user-circle<br />Academic<br />partner]
up2[fa:fa-user-circle<br />EFPIA<br />partner]
user[fa:fa-user-astronaut<br />Data<br />explorer]
  subgraph DUU[ ]
    cohort(fa:fa-desktop Cohort<br />metadata)
    dish(fa:fa-database DISH or <br />Appendix B)
    data(fa:fa-desktop Clinical <br />& omics<br />data)
    meta(fa:fa-desktop Clinical <br />& omics<br />metadata)
    vbb(fa:fa-desktop Sample<br />metadata)
  end
  subgraph DU[ ]
    RC(fa:fa-cubes Cohort<br />REDCap EDC)
    DUMA(fa:fa-desktop Data<br />storage)
    VB(fa:fa-cubes Virtual<br />biobank<br/>REDCap EDC)
  end
  subgraph DCZ[ ]
    catalog[fa:fa-database<br />Cohort catalog]
    tm[fa:fa-desktop<br />tranSMART]
    ada[fa:fa-desktop<br />Ada]
  end
up1 -.-  cohort
up1 -.-  dish
up1 -.-   data
up1 -.-   meta
up2 -.-  vbb
up2 -.-   cohort
up2 -.-   dish
up2 -.-  data
up2 -.-   meta
up2 -.-   vbb
cohort --> |"fa:fa-upload<br />----"| RC
dish --> |"fa:fa-upload<br />----"| RC
data --> |"fa:fa-upload <br />----"|DUMA
meta --> |"fa:fa-upload <br />----"|DUMA
vbb --> |"fa:fa-upload <br />----"| VB
RC --> catalog
DUMA --> catalog
DUMA --> tm
DUMA --> ada
VB --> ada
catalog -.- |"fa:fa-download <br />----"| user
tm -.- |"fa:fa-download <br />----"| user
ada -.- |"fa:fa-download <br />----"| user    


```
<i class="fas fa-hands-helping"></i> : Login with LUMS is required<br />
<i class="fas fa-upload"></i> : File upload from web-browser<br />
<i class="fas fa-link"></i> : Upload through DUMA Link
{: .fs-2 .text-right}

<style>
path:not([marker-end="url(#flowchart-pointEnd)"]) {
  stroke-dasharray: 3;
}
</style>

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## ImmUniverse-LUMS account

<span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span>is LCSB's User Management System for accessing the ImmUniverse data platform services including the cohort catalog, REDCap, GitLab, Ada and tranSMART. ImmUniverse members can make a request for a LUMS account using the following link. Account holders can use the same link for their account reactivation or password change request.

[Create ticket](https://immuniverse-redcap.lcsb.uni.lu/redcap/surveys/?s=D9334AF8FW){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }{:target="new"}

---

## ImmUniverse data capture and upload

> "Data and knowledge generated within ImmUniverse project [...] will be shared and made accessible through a secure and encrypted channel.” ―(from the ImmUniverse proposal).

WP2, WP3 and WP6 has set up a number of services to facilitate providing data in a structured way and complying with legal requirements. Detailed instructions and access to the upload platforms are summarised in this section. Firstly, the cohort will be generated for the future omics uploading. 

### Cohort metadata upload
Data owners need to fill the detail of data cohort information before uploading the data to this platform.

[Cohort metadata upload](https://immuniverse-redcap.lcsb.uni.lu/redcap){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }



### Data and metadata upload

{: .no_toc .d-inline-block}
coming soon
{: .label .label-yellow }

Data owners will upload their data to UL’s data platform using encrypted transfer through the Download Upload Manager (DUMA) service at UL. DUMA can be accessed through a web-browser or a command line interface. To upload data please follow the detailed instruction below. The detail of uploading data format and request will come soon.

[Data upload]({% link dataupload.md %}){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

### ImmUniverse Virtual Biobank

{: .no_toc .d-inline-block}
coming soon
{: .label .label-yellow }

To develop a common standard in the ImmUniverse Consortium for biobanks� biospecimen summary data, collectively known as sample-metadata, a catalog solution has been created for sample-metadata and a generic workflow for sample requests will be added shortly.

[Biobanking metadata]({% link biobank.md %}){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }

{: .no_toc .d-inline-block}
coming soon
{: .label .label-yellow }
---

## ImmUniverse data access

ImmUniverse Consortium members can access previously uploaded datasets and run analysis through web-based interfaces such as the  cohort catalog, tranSMART and ADA. They will be able to perform cross-study comparisons; slice and dice the cohorts based on certain clinical features and run built-in workflows. The raw data can also be downloaded locally for analysis.

### ImmUniverse cohort catalog

{: .no_toc .d-inline-block}
coming soon
{: .label .label-yellow }

ImmUniverse cohort catalog displays summary cohort information and enables consortium members to download raw and processed data where available. Consortium members can search, filter, and select datasets based on various parameters.

[Cohort catalog](https://ImmUniverse-cohort-catalog.uni.lu){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }
Login with <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span>

### ImmUniverse tranSMART

tranSMART is a modular open source software platform for querying, exploration and analysis of clinical, translational and omics data.

[tranSMART](https://ImmUniverse-transmart.lcsb.uni.lu){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2}
Login with <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span>

### ImmUniverse Ada

Ada is a performant and highly configurable system for secured integration, visualisation, and collaborative analysis of heterogeneous data sets.

[Ada](https://ImmUniverse-ada.lcsb.uni.lu/){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 .d-inline-block}
Login with <span class="label label-blue"><i class="fas fa-hands-helping"></i> LUMS </span>


