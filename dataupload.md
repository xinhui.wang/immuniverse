---
layout: default
title: Data Upload
nav_order: 2
has_children: true
permalink: /dataupload
---

# Data Upload

Work packages 2, 3, 5 and 6 include the activity of merging clinical and omics data as well as the experimental and sample level metadata for existing and upcoming patient and population cohorts within ImmUniverse.


```mermaid

flowchart LR
  A(fa:fa-user-circle Data<br />uploader) -->B1[fa:fa-file-archive Data & <br />metadata]
  A -->  B2[fa:fa-file-excel DISH or<br />Appendix B]

  B1 --> |Encrypted<br />data transfer| C((Data<br />platform))
  B2 --> |Encrypted<br />data transfer| D(("Data<br />Information<br />System<br />(DAISY)"))
  subgraph UL
  C --> E((Data and<br />computing<br />environment))
  D --> E
  end
  E --- |Access<br />control| F(fa:fa-user-astronaut Data<br />explorer)
  A -.- |Consortium level MoU or<br />Multiparty data sharing agreement| F(fa:fa-user-astronaut Data<br />explorer)

```

Figure 1 – ImmUniverse Data Transfer Overview:  The figure depicts the overview of data transfers between data owner/explorer and data and computing environment.

The ImmUniverse data upload guide provides instructions how to upload the data. The ImmUniverse metadata upload guide explains how to upload metadata about ImmUniverse cohorts. The ImmUniverse biobanking data guide is for uploading biobanking or sample level metadata.


[Cohort metadata upload](https://immuniverse-redcap.lcsb.uni.lu/redcap/){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }{:target="new"}
